# IGWN Rucio Guide

<https://rucio.docs.ligo.org/guide/>

This project uses [MkDocs](https://www.mkdocs.org) to build a
documentation website for Rucio in IGWN.

See <https://rucio.docs.ligo.org/guide/contributing/> for details
on how you can contribute to the guide project.
