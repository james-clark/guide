---
title: File transfer configurations
---

# File transfer configurations

This section (will) document e.g.:
- RSE file server locations / relevant configurations
- FTS information
- File transfer throttles


## File transfer throttles

The number of file transfer jobs submitted to FTS can be limited by setting
RSE-specific transfer limits.  When configured correctly and an RSE transfer
limit is set, all transfer requests to that RSE initially enter a `WAITING`
state.  The conveyor daemons update as many request states to `QUEUED` as
permitted by the limit, and the jobs get submitted to FTS.  All other requests
remain in the `WAITING` state until the `QUEUED` requests have completed.

!!! info "Throttling changes in newer Rucio releases"

    The examples documented below pertain to the Rucio 1.29.x series.
    Throttling configuration and usage has been reworked for Rucio 32.x.x.
    This documentation should be updated when we upgrade.

!!! warning "Client-created rules do not respect throttles"

    Note that transfer requests arising from replication rules created directly
    via the Rucio *client* API do **not** respect RSE transfer limits!  Those
    transfer requests immediately enter the `QUEUED` state.

    Rules which are generated from the rucio core API, as is the case for
    asynchronous rule creation from the `judge-injector` or automatic rule
    creation from the `transmogrifier` daemons, **do** respect transfer limits.

First, ensure that the `rucio-conveyor-preparer` daemon is deployed and that
`use_preparer=True` in the `conveyor` section of the `rucio.cfg` for direct
deployments or the `config` section of the daemons' `values.yaml` for
Helm-based deployments:

```yaml
## common config values used to configure the Rucio daemons
config:
  conveyor:
    use_preparer: True
```

The python commands which follow should be executed from a full Rucio
installation (i.e., a location with the full `rucio` package, not just
        `rucio-clients`).  This is most easily accessed from whereever a daemon
is running with a correctly configured client.  E.g. from inside one of the
daemon pods in the `ligo-rucio` k8s deployment:

```shell
$  kubectl exec -it rucio-daemons-conveyor-throttler-64cf88877d-5zgxp -- bash
```

To limit the number of transfers submitted to FTS with destination RSE
`LIGO-LA`:

```python
>>> from rucio.core.rse import set_rse_transfer_limits
>>> set_rse_transfer_limits(rse_id='a17cc1b1172e4671b87840a4ef959791', activity='User Subscriptions', max_transfers=100)
```

Where: 

- The `rse_id` can be found from e.g.:

    ```shell
    $  rucio-admin rse info LIGO-LA | grep rse_id
        rse_id: a17cc1b1172e4671b87840a4ef959791
    ```

- The `activity` field limits the throttle to transfers created from replication rules / subscriptions with that activity type.

Finally, we also set the configuration table values:

```shell
$  rucio-admin config set --section throttler --option 'User Subscriptions,LIGO-LA' --value 100
```
