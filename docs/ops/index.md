---
title: LIGO Rucio operations
---

# LIGO Rucio operations

This section of the documentation deals with material that operators/users of
the LIGO Rucio environment would require to manage data sets.

## Use case: Gravitational wave frame files

The primary use case for the LIGO Rucio instance is bulk management of
["offline" gravitational wave frame
files (GWF)](https://computing.docs.ligo.org/guide/data/).  Objectives are
to:

1. Maintain an exhaustive catalog of validated frame files including, but not
   necessarily limited to, types raw, trend, RDS (where applicable) and
   aggregated strain (aka h-of-t).
2. Replicate frame file datasets between the observatories and to the Caltech
   archive / OSDF data origin.

Other use cases will be documented on these pages as they are implemented.

### GWF management workflow

![drawing](https://docs.google.com/drawings/d/e/2PACX-1vQcI7xl31Pe9j4OjdORyl_Eh9aYI3e_1GsGHaTEuEBjygDfINkJtA9o1kfrIlPEN9avY5Mubk0fbdkY/pub?w=1440&h=1080)
