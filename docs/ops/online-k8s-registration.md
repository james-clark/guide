---
title: k8s GWF registration
---


# Online registration with k8s deployments

Online registration, where GWF file DIDs and metadata are discovered and
recorded in near-real time, can be managed via Kubernetes deployments of
`GravCat` instances.

Typically, we use one k8s deployment per frame file type.

The necessary ingredients are:

- Rucio client configuration file (e.g. `rucio.cfg`) stored as a
  [k8s configmap](https://kubernetes.io/docs/concepts/configuration/configmap/).
- A`GravCat` configuration file for the given frame type (e.g. `h1-hoft.ini`)
  stored as a k8s configmap.
- Client SSH credentials stored as a [k8s
  secret](https://kubernetes.io/docs/concepts/configuration/secret/).
- Client X509 cert and key stored as k8s secrets.
- Client deployment manifest.
- A service account to allow automatic creation of k8s secrets (used for X.509
    proxy renewal).
- A k8s cronjob to renew X.509 credentials.
- Optionally: a k8s cronjob to periodically restart `GravCat` pods.  This also
  requires a k8s service account.  To parallel upstream Rucio Helm chart
  deployments, we use separate service accounts for the restart and proxy
  renewal jobs.

## Kustomization file

All of the above can be managed with a
[kustomization](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/)
file, which allows common configurations and secrets to be shared between
`GravCat` deployments.

!!! example "Minimal kustomization.yaml"

    ```yaml
    ---
    apiVersion: kustomize.config.k8s.io/v1beta1
    kind: Kustomization

    configMapGenerator:
    # GravCat client configuration file (Dataset configuration)
    - files:
      - l1-hoft.ini
      name: gravcat-l1-raw-ini
    # Rucio client configuration file (Rucio server names etc)
    - files:
      - rucio.cfg
      name: rucio-cfg

    secretGenerator:
    # SSH credentials for frame file validation
    - files:
      - secrets/id_rsa
      - secrets/id_rsa.pub
      name: client-ssh-key
    # X509 credentials for remote metadata computation
    - files:
      - secrets/usercert.pem
      name: rucio-clients-x509-cert
    - files:
      - secrets/new_userkey.pem
      name: rucio-clients-x509-key
    generatorOptions:
     disableNameSuffixHash: true

    resources:
      # GravCat pod deployment manifest
      - l1-hoft.yaml
      # K8s service account for secret creation
      - rucio-clients-rucio-edit.yaml
      # K8s cronjob to renew X509 proxy
      - renew-client-proxy-cronjob.yaml
      # K8s cronjob to renew X509 proxy
      - rucio-clients-rucio-restart.yaml
      - automatic-restart-cronjob-ligo-la.yaml
    ```

## GravCat deployment manifest

When running in online mode as a k8s deployment, each pod has two containers:

1. A `gfal2-utils` container: used to periodically fetch the `DiskCache` dump
   from the target RSE.
1. The `GravCat` client container: parses the `DiskCache` dump to look for new
   files, performs frame validation over SSH, remotely computes file metadata
   and registers new replicas in the database via the Rucio client API.

Credentials, stored as secrets, and configurations, stored as configmaps, are
mounted at appropriate paths defined in the GravCat deployment manifest.

!!! example "GravCat deployment manifest"

    ```yaml
    ---
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: gravcat-l1-hoft
      labels:
        ifo: l1
        type: hoft
        site: ligo-la
    spec:
      selector:
        matchLabels:
          ifo: l1
          type: hoft
          site: ligo-la
      template:
        metadata:
          labels:
            ifo: l1
            type: hoft
            site: ligo-la
        spec:
          containers:
          - name: stage-data
            image: containers.ligo.org/computing/rucio/containers/gfal2-utils:latest
            resources:
              limits:
                cpu: 1
                memory: 1Gi
              requests:
                cpu: 100m
                memory: 100Mi
            volumeMounts:
            - name: data
              mountPath: /data
            - name: proxy-volume
              mountPath: /tmp
            env:
              - name: X509_USER_PROXY
                value: "/tmp/x509up"
              - name: SRC
                value: "gsiftp://ldas-llo.ligo-la.caltech.edu:2811/var/lib/diskcache/frame_cache_dump"
              - name: DEST
                value: "/data/diskcache.ldas"
              - name: DAEMON
                value: "True"
              - name: SLEEP_INTERVAL
                value: "600"
            lifecycle:
              postStart:
                exec: # Wait for DiskCache dump to show up from sidecar container
                  command: ["/bin/sh", "-c", "until test -f ${DEST}; do echo Waiting for DiskCache: ${DEST}; sleep 3; done"]
          - name: rucio-clients
            image: containers.ligo.org/computing/rucio/containers/rucio-clients:release-1.29.11
            command: ["/bin/bash", "-ce", "gravcat --config-file ${CONFIG} --pfns-file ${DISKCACHE} --rse ${RSE} register --sleep-interval 600 --online"]
            imagePullPolicy: Always
            resources:
              limits:
                cpu: 1
                memory: 0.5Gi
              requests:
                cpu: 1
                memory: 100Mi
            env:
              - name: X509_USER_PROXY
                value: "/opt/proxy/x509up"
              - name: CONFIG
                value: "/opt/rucio/registrar/l1-hoft.ini"
              - name: DISKCACHE
                value: "/data/diskcache.ldas"
              - name: RSE
                value: "LIGO-LA"
              - name: GRAVCAT_STATE_DIR
                value: "/opt/gravcat-state"
              - name: RUCIO_ACCOUNT
                value: "frames"
            volumeMounts:
            - name: gravcat-ini
              mountPath: /opt/rucio/registrar
            - name: rucio-cfg
              mountPath: /opt/rucio/etc
            - name: ssh-key
              readOnly: true
              mountPath: /home/user/.ssh
            - name: proxy-volume
              mountPath: /opt/proxy
            - name: data
              mountPath: /data
            securityContext:
              runAsUser: 1000
          securityContext:
            fsGroup: 1000
          volumes:
          - name: ssh-key
            secret:
              secretName: ssh-key
          - name: proxy-volume
            secret:
              secretName: rucio-clients-rucio-x509up
              defaultMode: 420
          - name: rucio-cfg
            configMap:
              name: rucio-cfg
          - name: gravcat-ini
            configMap:
              name: gravcat-l1-hoft-ini
          - name: data
            emptyDir: {}
    ```


