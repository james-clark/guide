---
title: LIGO Rucio configuration
---


This section of the documentation deals with material that administrators and
operators of the LIGO Rucio environment would require to prepare the Rucio
instance for use.  For example: database schema initialisation, account setup
and Rucio storage element configurations.  This also contains details regarding
namespace (Rucio scopes) setup, naming conventions and metadata initialisation.

Use the navigation bar at the left and links on this page to
learn about:

* [__Configuration walkthrough__](./walkthrough.md): A TL;DR overview of the
  initial Rucio instance with step-by-step instructions to configure a new Rucio
  installation using pre-canned scripts.
* [__Accounts__](./accounts.md): detailed information on user account
  configurations.
* [__Storage elements__](./rses.md): detailed information on Rucio Storage
  Elements (RSEs) - end-points for data transfers - in use by the LIGO Rucio
  instance.
* [__Metadata__](./meta.md): configuration, conventions and examples for adding
  metadata to DIDs.
* [__Scopes and names__](./scopes.md): configuration, conventions and examples
  for Rucio namespace scopes.

!!! tip "Quickstart"
  
    The [IGWN Rucio
    Configuration](https://git.ligo.org/computing/rucio/igwn-rucio-config)
    repository contains a
    [`kustomization.yaml`](https://git.ligo.org/computing/rucio/igwn-rucio-config/-/blob/main/kustomization.yaml)
    which creates k8s configmaps for initial RSE and account configurations and
    launches a k8s job which creates accounts, RSEs and initialises some global
    metadata.

    Deploy from the [IGWN Rucio
    Configuration](https://git.ligo.org/computing/rucio/igwn-rucio-config)
    repository in the usual manner:

    ```shell
    $  kubectl apply -k .
    ```
