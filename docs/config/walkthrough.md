---
title: LIGO Rucio configuration walkthrough
---

A TL;DR overview of the initial Rucio instance with step-by-step instructions
to configure a new Rucio installation using pre-canned scripts.
