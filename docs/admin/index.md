---
title: Administration
---


This documentation describes how and where the LIGO instance is deployed, and
provides a reference for specific configurations and troubleshooting tips.

!!! info "Rucio components and concepts"

    A complete overview of Rucio [_services and
    components_](https://rucio.github.io/documentation/started/main_components/)
    can be found in the [_upstream Rucio
    documentation_](https://rucio.github.io/documentation) and in the [_Rucio
    publication_](https://link.springer.com/article/10.1007/s41781-019-0026-3).

Use the navigation bar at the left and links on this page to
learn about:

* [__Nautilus k8s deployment__](./nrp.md): access to and use of the Nautilus k8s
  cluster.
* [__Deployment walkthrough__](./walkthrough.md): step-by-step instructions to
  standing up and initialising a fresh Rucio instance on NRP.
* [__Database__](./database.md): reference notes / troubleshooting tips for the
  backend SQL database and tips on direct access.
* [__Server__](./server.md): reference notes / troubleshooting tips for the API
  and Authentication http servers.
* [__Daemons__](./daemons.md): reference notes / troubleshooting tips for
  important Rucio daemons currently used by the LIGO deployment.
* [__FTS__](./fts.md): information regarding the externally-hosted File
  Transfer Service instances available for use by Rucio.
* [__Monitoring__](./monit.md): descriptions of available monitoring.
* [__Policy package__](./policy.md): LIGO customisations for account
  permissions and schemas.
* [__LFN-to-PFN algorithms__](./lfn2pfn.md): Conventions and algorithms for
  deriving physical filenames from logical filesnames.
