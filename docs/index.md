---
hide:
  - toc
title: Welcome
---

# Welcome to the LIGO Rucio guide

[Rucio](https://rucio.cern.ch/) is a project used by the International
Gravitational Wave Network (IGWN) to provide services and associated libraries
for centralised management of large volumes of archival data (E.g.,
gravitational wave frame files and products derived therefrom).

LIGO and Virgo both manage independent Rucio instances.  These pages regard
administration and operation of the LIGO Rucio deployment.

!!! question "Is this site for you?"

    These pages attempt to document adminstration and operation of the LIGO
    Rucio instance.  This is not (currently) a user-facing service - if you're
    looking for information on IGWN data access for analysis pipelines, you
    will probably be better served by
    <https://computing.docs.ligo.org/guide/data/>

!!! info "Getting help"

    If you came here looking for help with a problem, please see
    [_Help and support_](./help.md).

This documentation aims to be as complete as possible, however more
information may be found on other sites, including the [LIGO Rucio gitlab
wiki](https://git.ligo.org/groups/computing/rucio/-/wikis/home).

## Documentation structure


This documentation is split into the following main sections, accessible via
the navigation bar at the top of this page:

* [__Admininistration__](./admin/index.md):  material that administrators of
the LIGO Rucio environment would require to stand up and maintain a Rucio
deployment. For example, how and where the LIGO Rucio instance is deployed,
configurations for Rucio service components and details regarding the Rucio
Docker containers we use.
* [__Configuration__](./config/index.md):  material that administrators and
operators of the LIGO Rucio environment would require to prepare the Rucio
instance for use.  For example: database schema initialisation, account setup
and Rucio storage element configurations.  This also contains details regarding
namespace (Rucio scopes)
setup, naming conventions and metadata initialisation.
* [__Data Management & Operations__](./ops/index.md): material that
operators/users of the LIGO Rucio environment would require. For example, basic
client operations, how data is discovered and registered (aka "published"),
client configurations and deployments, and replication workflows.


## External links


- [Rucio website](https://rucio.cern.ch/)
- [Upstream Rucio documentation](https://rucio.github.io/documentation/)
- [Rucio github projects](https://github.com/rucio/rucio)
- [Official Rucio support channels](https://rucio.cern.ch/documentation/contact_us/)
