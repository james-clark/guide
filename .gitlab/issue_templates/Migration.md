#### Details

<!-- add some text here to describe the doc(s) being migrated -->

**Source:** <!-- add link to current page -->

**Target:** [fixme.md](docs/fixme.md) / new page <!-- delete as appropriate-->

#### Tasks

- [ ] create/update guide page(s)
- [ ] replace wiki page(s) with stub(s)

/epic computing&1
/milestone %"O4 Advance"
